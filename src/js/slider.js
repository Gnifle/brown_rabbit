$( document ).ready( function() {

	$( '#slider' ).bxSlider({
		auto:       true,
		autoStart:  true,
		autoHover:  true,
		controls:   false,
		pause:      5000
	});

	$( window ).on( 'resize', function() {

		$('.excerpt').shave( 60 );

	});

	$( window ).trigger( 'resize' );

});